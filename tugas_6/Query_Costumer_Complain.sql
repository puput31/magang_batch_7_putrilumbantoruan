
/ menghitung jumlah komplain setiap bulan/
SELECT DATE_FORMAT(date_received, "%m") AS MONTH, 
COUNT(complaint_id) AS Jumlah 
FROM consumercomplaints
GROUP BY DATE_FORMAT(date_received, "%m")
ORDER BY DATE_FORMAT(date_received, "%m")

/Komplain yang memiliki tags 'Older American'/
SELECT complaint_id, tags FROM consumercomplaints
WHERE tags = "Older American"

/Buat sebuah VIEW yang menampilkan DATA nama perusahaan, jumlah company response TO consumer/
SELECT company, SUM(untimely_response) AS "Untimely Response",
	SUM(closed) AS Closed,
	SUM(closed_exp) AS "Closed with explanation",
	SUM(closed_non) AS "Closed with non-monetary relief"
FROM(
	SELECT company, 
		CASE company_response_to_consumer
			WHEN "Untimely Response" THEN Jumlah
			ELSE 0
		END AS untimely_response,
		
		CASE company_response_to_consumer
			WHEN "Closed" THEN Jumlah
			ELSE 0
		END AS closed,
		
		CASE company_response_to_consumer
			WHEN "Closed with explanation" THEN Jumlah
			ELSE 0
		END AS closed_exp,
		
		CASE company_response_to_consumer
			WHEN "Closed with non-monetary relief" THEN Jumlah
			ELSE 0
		END AS closed_non
	FROM(
		SELECT company, company_response_to_consumer, COUNT(company_response_to_consumer) AS Jumlah
		FROM consumercomplaints
		GROUP BY company
	) AS m
		
) AS n
GROUP BY company
ORDER BY company;