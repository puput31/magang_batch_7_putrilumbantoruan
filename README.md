Fungsi penggunaan git pada project adalah untuk memudahkan programmer dalam mengetahui
perubahan-perubahan dari source code yang dibuat untuk aplikasinya. Selain itu, dengan
menggunakan git sorang programmer juga dapat bekerja secara berkelompok tanpa perlu
khawatir codenya akan bentrok, karena setiap orang dapat membuat branch sebagai halaman kerjanya.
